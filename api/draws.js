const express = require('express');

const Draw = require('./Draw');

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
      Draw.find()
      .then(results => {
          let data={
            line: results[0].line
          };
          let arr=[];
          for (let i=1; i<results.length; i++){
              arr = data.line.concat(results[i].line);
              data.line = arr;
              arr = [];
          }
          res.send(data)
      })
      .catch(() => res.sendStatus(500));
  });


  return router;
};

module.exports = createRouter;
