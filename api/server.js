const express = require('express');
const cors = require('cors');
const app = express();
const expressWs = require('express-ws')(app);
const mongoose = require('mongoose');

const config = require('./config');
const Draw = require('./Draw');
const draws = require('./draws');

const port = 8000;

app.use(cors());

const clients = {};

app.ws('/draw', (ws, req) => {
  const id = req.get('sec-websocket-key');
  clients[id] = ws;

  console.log('Client connected.');
  console.log('Number of active connections: ', Object.values(clients).length);


  ws.on('message', (msg) => {
   try {
       const image = new Draw(JSON.parse(msg));
       image.save().then(
        Object.values(clients).forEach(client => {
            client.send(msg)}));
    } catch (e) {
        console.log(e);
    }
  });

  ws.on('close', (msg) => {
    delete clients[id];
    console.log('Client disconnected.');
    console.log('Number of active connections: ', Object.values(clients).length);
  });
});
mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
    console.log('Mongoose connected!');

    app.use('/draws', draws());


    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
