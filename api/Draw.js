const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DrawSchema = new Schema({
  line: {
    type: Array,
    required: true,
  }
});
const Draw = mongoose.model('Draw', DrawSchema);

module.exports = Draw;
